# Eochat-QT

A graphical user interface for Everfree Outpost

## Install

To install:

``` bash
pip install eochat-qt
```

Then run with `eochat-qt`


## Potential Features

Potential suggested features for eochat-qt:

Ordered in terms of easiness:

- Better message send blocking io
    - Send program signal handlers to interrupt asyncio sleep?
    - Disable GIL?
- Autoreconnect on disconnect
    - When the keep alive fails, we should try to send another request for connection again.
- Play notification sound on new message
    - Can toggle on/off
- Online Players Widget
- Colored player chats
    - Color just the player name for now (might be hard to read if the entire chat line was colored?).
    - Read a JSON key/dict with names of players and specified color codes/ASCII colors.
- Ziggy Command Tab
- Nearby Players Widget
    - Works because the server sends:
        - 0x800c when in local range
        - 0x800d when outside of local range
- Android support
- Toggleable Preview Window Widget
