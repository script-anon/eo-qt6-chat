# Asyncio Loop

## Efficient Execution Model

To get the fastest execution model for asyncio we want to make use of
a single thread as much as possbible while eliminating downtime, but still keeping
the app "responsive".

### Tasks

- Sending message to server
    - When we receive an event to send a message to the server,
        send the message immediately, and wait for more messages to be sent.
- Receiving messages from the server
    - As long as we're connected to the server, await for further messages,
        read and display the response.
- Sending keep alives to the server
    - As long as we're connected to the server, send keep-alive pings.
- Sending /who pings to the sever
    - As long as we're connected to the server, send /who pings every 10 minutes,
        and update the online players widget.
- Run the GUI
    - As long as we haven't exited the program or ran into a fatal error, run the graphical user interface.

These tasks when sorted from the greatest life span duration (or longest execution time) are as follows:

- Run the GUI
- Receiving & reading messages from the server
- Sending keep alives to the server
- Sending /who pings to the sever
- Sending message to server

At the minimum we require two threads:
- One to run the GUI (must be on the main thread)
- One to run the async tasks
