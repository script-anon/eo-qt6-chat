# Eochat
from eochat.periodic import iping
from eochat.processer import decode_message, CHAT_UPDATE
from eochat.builder import build_chat, build_appearance

# Eochat-QT
from eochat_qt.helpers import timestamp
from eochat_qt.api import do_login, do_auth, EncryptedSocket
from eochat_qt.config import get_log_name, default_to
from eochat_qt.desktop.widgets.nearbywidget import PLAYER_NEARBY, PLAYER_FARAWAY

from eochat_qt.desktop.widgets.mainwidget import MainWidget
import eochat_qt.config

# Third Party Libaries
from loguru import logger
from PySide6.QtWidgets import QApplication
from tornado.platform.asyncio import to_asyncio_future

# Standard Library Imports
import asyncio
import threading
import os
import sys
import struct

logger.remove() # Override default logger
# Format: [2022-09-01 23:36:01.792] [DEBUG] [eochat_qt.main:150] Hello!
PROGRAM_LOG_MSG_FORMAT = '\x1b[0m\x1b[32m[{time:YYYY-MM-DD HH:mm:ss.SSS}]\x1b[0m [<lvl>{level}</>] [<c>{name}:{line}</>] {message}'
loglevel = default_to('ERROR', os.environ.get('LOGLEVEL'))
logger.add(sys.stdout,format=PROGRAM_LOG_MSG_FORMAT, level=loglevel)

# Global Constants
SERVER_MSG_ID = '***'                               # Server messages always contain *** at the start of the message
SERVER_MSG_PING_ID = 'Currently online: '           # Server /who responses contain this string identifier
SERVER_PLAYER_PING_OFFSET = len(SERVER_MSG_PING_ID) # Server /who responses contain the player names after this offset
CLIENT_PING_PERIOD = int(10 * 60)                   # Client will ping server for new players every 10 minutes

# Create singleton instances
CONFIG = eochat_qt.config.CONFIG
LOG_CONFIG = eochat_qt.config.LOG_CONFIG

# Start up the GUI
# This must be started before the mainwindow call
app = QApplication(sys.argv)

# Global singleton
MAIN_WIDGET: MainWidget

def format_msg(tsformat, msg):
    ts = timestamp(tsformat)
    ts = ts.strip('\'').strip('\"')
    return f'[{ts}] {msg}'

def log_chat_msg(log_fmt):
    ''' Logs player messages to a file
        Rotates on every new day
    '''
    global LOG_CONFIG
    if LOG_CONFIG.logging:
        current_day = f'chat-{timestamp(LOG_CONFIG.LOG_FILE_FMT)}'
        # Rotate log if its already new day
        if (current_day > LOG_CONFIG.log_name):
            # Lexicographically compare these formats
            # Since they're ordered like this:
            # d1: chat-2022-08-25
            # d2: chat-2022-08-26
            # We can directly compare them across to find out if a new day has passed
           LOG_CONFIG.log_name = current_day # Log to a new file
           LOG_CONFIG.logfp = get_log_name(LOG_CONFIG.log_name)

        # Log messages to file
        with open(LOG_CONFIG.logfp, 'a') as log:
            log.write(log_fmt)
            log.write('\n')

def display(msg):
    ''' Log a message to stdout, and the gui'''
    global MAIN_WIDGET, LOG_CONFIG
    log_fmt = format_msg(LOG_CONFIG.LOG_TIMESTAMP_FMT, msg)
    fmt     = format_msg(LOG_CONFIG.tsformat, msg)

    # Display
    print(fmt)
    MAIN_WIDGET.chatwidget.new_message(fmt)

    # Log player message
    log_chat_msg(log_fmt)

def get_ui_input():
    global MAIN_WIDGET
    return MAIN_WIDGET.chatwidget.send_message()

def is_server_message(msg):
    msg_start_id = msg[0]
    return True if msg_start_id == SERVER_MSG_ID else False

def is_server_player_ping(server_msg_conts):
    server_online_ping = server_msg_conts[0:SERVER_PLAYER_PING_OFFSET]
    return True if server_online_ping == SERVER_MSG_PING_ID else False

def update_online_players_widget(msg):
    global MAIN_WIDGET
    server_msg_conts = msg[1]

    logger.debug(f'Server Message Response: {msg}')

    if is_server_message(msg) and is_server_player_ping(server_msg_conts):
        online_players_str  = server_msg_conts[SERVER_PLAYER_PING_OFFSET:]
        logger.debug(f'Online Players: {online_players_str}')

        # Split the players names up
        online_players_list = online_players_str.split(',')

        # Strip all whitespace to the start and end of the player's name
        online_players = map(lambda p: p.lstrip().rstrip(), online_players_list)
        MAIN_WIDGET.onlinewidget.online = online_players
        MAIN_WIDGET.onlinewidget.update_online_players_list()

def update_nearby_players_widget(op, player_name):
    global MAIN_WIDGET
    logger.debug(f'Player: {player_name}')

    if op == PLAYER_NEARBY:
        MAIN_WIDGET.nearbywidget.nearby.push_player(player_name)
    elif op == PLAYER_FARAWAY:
        MAIN_WIDGET.nearbywidget.nearby.pop_player(player_name)

    MAIN_WIDGET.nearbywidget.update_nearby_players_list()

async def sender(conn):
    global MAIN_WIDGET
    try:
        while True:
            # TODO: Fix this to become a nonblocking call
            # await asyncio.sleep(5)
            await asyncio.sleep(1)
            while MAIN_WIDGET.chatwidget.message_obtained:
                msg = get_ui_input()
                await conn.send(build_chat(msg))
                MAIN_WIDGET.chatwidget.message_obtained = False
                MAIN_WIDGET.chatwidget.clear_input()
                break;
    finally:
        asyncio.get_event_loop().stop()

async def receiver(conn):
    try:
        while True:
            msg = await to_asyncio_future(conn.recv())
            cached_msg = msg
            if msg is None:
                display('Disconnected from server')
                assert False, 'server disconnected'
            op, msg = decode_message(msg)

            if op == CHAT_UPDATE:
                # Indicate local chat messages with (local)
                msg = msg.split('\t')
                msg[2] = ' (local)' if (msg[2] == '27686') else ''
                display(f'{msg[0]}{msg[2]}: {msg[1]}')

                update_online_players_widget(msg)

            if (op == PLAYER_NEARBY) or (op == PLAYER_FARAWAY):
                print(struct.unpack_from('HHHHHHH',cached_msg,0))
                player_name = str(cached_msg[16:].decode("utf_8"))

                msg_tuple = struct.unpack_from('HHHHHHH',cached_msg,0)+(cached_msg[16:].decode("utf_8"),"")
                update_nearby_players_widget(op, player_name)

    finally:
        asyncio.get_event_loop().stop()

async def ping_online(conn, time):
    ''' Sends /who commands periodically to the server

    This will ping the server for the online players but, receiver will deal with updating the online widget
    '''
    global MAIN_WIDGET
    try:
        while True:
            # Send the ping command
            await conn.send(build_chat('/who'))

            # Wait for some time
            await asyncio.sleep(time)
    finally:
        asyncio.get_event_loop().stop()

async def amain(user, password):
    global CONFIG
    display('Logging in as %r...' % CONFIG.user)
    cookie = await to_asyncio_future(do_login(user, password))
    display(f'Cookie: \'{cookie}\'')
    display('Connecting...')

    conn = EncryptedSocket()
    await conn.connect(CONFIG.GAME_URL)
    display(f'Challenge: \'{conn.cb_token}\'')
    display('Authenticating...')

    name = await do_auth(conn, cookie)
    display(f'Connected as {name}')

    await conn.send([22,0]) # Send start header
    await conn.send(build_appearance(*CONFIG.appearance)) # With custom appearance

    asyncio.get_event_loop().create_task(receiver(conn))
    asyncio.get_event_loop().create_task(sender(conn))
    asyncio.get_event_loop().create_task(iping(conn))

    asyncio.get_event_loop().create_task(ping_online(conn, CLIENT_PING_PERIOD))

    display('Connected to Server')

def main():
    global CONFIG, MAIN_WIDGET

    MAIN_WIDGET = MainWidget()
    MAIN_WIDGET.show()

    asyncio.get_event_loop().create_task(amain(CONFIG.user, CONFIG.password))

    # Start event loop on separate thread to main loop
    t = threading.Thread(target=asyncio.get_event_loop().run_forever, args=())
    t.daemon = True
    t.start()

    # App must be started on main thread
    app.exec()
