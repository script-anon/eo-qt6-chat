from datetime import datetime
from pathlib import Path
import toml
import platform

# Helper classes
# Singleton decorator
_register = {}

def singleton(cls):
   def wrapper(*args, **kw):
       if cls not in _register:
           instance = cls(*args, **kw)
           _register[cls] = instance
       return _register[cls]

   wrapper.__name__ = cls.__name__
   return wrapper

# def singleton(cls):
      # return cls()

# Singleton Metaclass
class Singleton(type):
    ''' Manage only a single global instance of this class across python modules '''
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

# Helper functions
def read_file(fname: str) -> str:
    file = open(fname, 'r')
    res = file.read()
    file.close()
    return res

def timestamp(fmt_str):
    ''' Format timestamps for the current datetime '''
    return datetime.now().strftime(fmt_str)

def read_cfg(cfgfp):
    return toml.loads(read_file(cfgfp))

def expand(path):
    return Path(path).expanduser()

def is_windows():
    return any(platform.win32_ver())
