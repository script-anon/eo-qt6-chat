# import eochat_qt.config
import eochat_qt.config
import eochat.crypto as crypto

import tornado.websocket
import tornado.platform.asyncio
import tornado.httpclient
from tornado.platform.asyncio import to_asyncio_future

from collections import deque
import base64
import struct
import urllib.parse
import json

# Definitions
OP_AUTH_RESPONSE    = 0x0014
OP_AUTH_CHALLENGE   = 0x8021
OP_AUTH_RESULT      = 0x8022

MODE_SSO            = 0
MODE_LOCAL          = 1

# Create singleton instances
CONFIG = eochat_qt.config.CONFIG
LOG_CONFIG = eochat_qt.config.LOG_CONFIG

async def do_login(user, password):
    global CONFIG
    auth_client = tornado.httpclient.AsyncHTTPClient()

    args    = { 'name': user, 'password': password}
    headers = { 'Host': CONFIG.AUTH_HOST, }
    resp = await auth_client.fetch(f'{CONFIG.AUTH_BASE}/login', method='POST',
                                   body=urllib.parse.urlencode(args), headers=headers,
                                   follow_redirects=False, raise_error=False)
    assert resp.code == 302
    cookie = resp.headers['Set-Cookie'].partition(';')[0]
    return cookie

async def get_response(challenge, cookie):
    global CONFIG
    auth_client = tornado.httpclient.AsyncHTTPClient()

    headers = {
        'Origin': CONFIG.AUTH_ORIGIN,
        'Host': CONFIG.AUTH_HOST,
        'Cookie': cookie,
    }
    body = json.dumps({
        'challenge': base64.urlsafe_b64encode(challenge).decode('ascii'),
    })
    resp = await auth_client.fetch(f'{CONFIG.AUTH_BASE}/api/sign_challenge', method='POST', body=body, headers=headers)
    j = json.loads(resp.body.decode('ascii'))
    assert j['status'] == 'ok'
    return base64.urlsafe_b64decode(j['response'].encode('ascii'))

async def do_auth(conn, cookie):
    response = await to_asyncio_future(get_response(conn.cb_token, cookie))
    await conn.send(struct.pack('<H', OP_AUTH_RESPONSE) + response)

    while True:
        msg = await to_asyncio_future(conn.recv())
        if msg is None:
            assert False, 'server disconnected unexpectedly'
        opcode, = struct.unpack('<H', msg[:2])

        if opcode == OP_AUTH_RESULT:
            flags, = struct.unpack('<H', msg[2:4])
            if flags == 1:
                name = msg[4:].decode('ascii')
                return name
            else:
                assert False, 'bad auth result: %s' % flags

        else:
            assert False, 'bad opcode: %x' % opcode

class EncryptedSocket:
    def __init__(self):
        self.conn = None
        self.proto = None
        self.recv_queue = deque()
        self.cb_token = None

    async def connect(self, url):
        self.conn = await to_asyncio_future(tornado.websocket.websocket_connect(url))
        self.proto = crypto.Protocol(initiator=True)

        # Perform handshake
        self.proto.open()
        while True:
            evt = self.proto.next_event()
            if evt is None:
                msg = await to_asyncio_future(self.conn.read_message())
                self.proto.incoming(msg)
            elif isinstance(evt, crypto.RecvEvent):
                self.recv_queue.append(evt.data)
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.HandshakeFinishedEvent):
                self.cb_token = evt.cb_token
                return
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unknown event type in connect: %r' % (evt,)

    async def recv(self):
        if len(self.recv_queue) > 0:
            return self.recv_queue.popleft()

        while True:
            evt = self.proto.next_event()
            if evt is None:
                msg = await to_asyncio_future(self.conn.read_message())
                self.proto.incoming(msg)
            elif isinstance(evt, crypto.RecvEvent):
                return evt.data
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unexpected event type in recv: %r' % (evt,)

    async def send(self, data):
        self.proto.send(data)

        while True:
            evt = self.proto.next_event()
            if evt is None:
                return
            elif isinstance(evt, crypto.RecvEvent):
                self.recv_queue.append(evt.data)
            elif isinstance(evt, crypto.OutgoingEvent):
                self.conn.write_message(evt.data, binary=True)
            elif isinstance(evt, crypto.ErrorEvent):
                raise ValueError('crypto error: %s' % evt.message)
            else:
                assert False, 'unexpected event type in send: %r' % (evt,)

