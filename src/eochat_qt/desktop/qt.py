# QT GUI Helper Functions

# Third Party Libraries
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile, QIODevice, QObject, Signal
from PySide6.QtWidgets import QApplication

# Standard Library
import sys

# Definitions
# Path to qt6 resource files directory
RESOURCES_DIR   = 'resources'
UI_MAIN_WINDOW   = f'{RESOURCES_DIR}/mainwindow.ui'
UI_ONLINE_WIDGET = f'{RESOURCES_DIR}/onlinewidget.ui'

def init_qt_uic_loader(filepath):
    ''' Load UI file directly from file '''
    ui_file = QFile(filepath)
    if not ui_file.open(QIODevice.ReadOnly):
        print(f"Cannot open {filepath}: {ui_file.errorString()}")
        sys.exit(-1)
    loader = QUiLoader()
    return loader, ui_file

def init_ui_file(loader, ui_file, constructor, *args, **kwargs):
    ''' Loads a ui resource file and constructs the class with the given
    constructor and optional args, kwargs. '''
    widget = constructor(loader.load(ui_file), args, kwargs)
    ui_file.close()
    if not widget:
        print(loader.errorString())
        sys.exit(-1)
    return widget

def toggle_qt6_checkbox(state):
    ''' Toggle between on/off states for a QT6 Checkbox '''
    return (True if state == 2 else False)

def show_widget(widget_name):
    ''' Testing function to display a widget '''
    app = QApplication(sys.argv)
    widget = widget_name()
    widget.show()
    app.exec()

class MessageBus(QObject):
    ''' Relay messages to/from QT widgets '''
    te_messages_received = Signal(bool)
    te_messages_updated = Signal(bool)
