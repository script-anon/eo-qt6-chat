# QT GUI Widget Definitions

# Imports
from eochat_qt.helpers import Singleton
from eochat_qt.config import NOTIFICATION_SOUND_WAV
from eochat_qt.desktop.qt import MessageBus, toggle_qt6_checkbox
import eochat_qt.config

# Custom Widgets
from eochat_qt.desktop.widgets.ui_chatwidget import Ui_ChatWidget

# Third Party Libraries
from openal import oalOpen, oalQuit

# PySide6
from PySide6.QtWidgets import QWidget
from PySide6.QtCore import Slot
from PySide6.QtWidgets import QApplication

# Singletons
CONFIG = eochat_qt.config.CONFIG
LOG_CONFIG = eochat_qt.config.LOG_CONFIG

class ChatWidget(QWidget):
    __metaclass__ = Singleton

    def __init__(self):
        global NOTIFICATION_SOUND, CONFIG, LOG_CONFIG
        super().__init__()
        self.ui = Ui_ChatWidget()
        self.ui.setupUi(self)

        # Members
        self.messages = list(str())
        self.mbus = MessageBus()
        self.message_obtained = False

        # When we receive a new message, show all new messages
        self.mbus.te_messages_received.connect(self.show_messages)

        # When we update messages (i.e the chatbox), emit new message signal
        self.mbus.te_messages_updated.connect(self.new_message)

        # Typing enter in input box sends message
        self.ui.le_input.returnPressed.connect(self.send_message)

        # Quit button
        self.ui.pb_quit.clicked.connect(QApplication.closeAllWindows)

        # TextEdit shifts focus to new message automatically
        self.mbus.te_messages_received.connect(self.scroll_textbox)

        # Update logging when checkbox is ticked
        self.ui.cb_logging.stateChanged.connect(self.toggle_logging)

        # Update enable_sound when checkbox is ticked
        self.ui.cb_enable_sound.stateChanged.connect(self.toggle_notify_sound)

        # Play notification sounds
        self.source = oalOpen(NOTIFICATION_SOUND_WAV)

        # Play notification sound on new message
        self.mbus.te_messages_received.connect(self.play_notify_sound)

    @Slot(int)
    def toggle_logging(self, state):
        global LOG_CONFIG
        LOG_CONFIG.logging = toggle_qt6_checkbox(state)

    @Slot(int)
    def toggle_notify_sound(self, state):
        global CONFIG
        CONFIG.enable_sound = toggle_qt6_checkbox(state)

    @Slot(bool)
    def play_notify_sound(self, _):
        global CONFIG
        if CONFIG.enable_sound:
            self.source.play()

    @Slot(bool)
    def new_message(self, message):
        self.messages.append(message)
        self.mbus.te_messages_received.emit(True)

    @Slot(bool)
    def show_messages(self):
         msgs = '\n'.join(self.messages)
         self.ui.te_messages.setText(msgs)

    def send_message(self):
        msg = self.ui.le_input.text()
        self.message_obtained = True
        return msg

    def scroll_textbox(self):
        self.ui.te_messages.verticalScrollBar().setValue(self.ui.te_messages.verticalScrollBar().maximum());

    def clear_input(self):
        self.ui.le_input.clear()

    def __del__(self):
        # When this widget is destroyed, do not receive any more notification sounds
        oalQuit()
