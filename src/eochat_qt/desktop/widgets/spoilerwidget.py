# Imports
from eochat_qt.desktop.qt import show_widget

# PySide6
from PySide6.QtCore import QParallelAnimationGroup
from PySide6.QtWidgets import (
    QPushButton,
    QWidget,
    QScrollArea,
    QFrame,
    QToolButton,
    QGridLayout,
    QVBoxLayout,
    QLabel,
)
from PySide6.QtGui import Qt
from PySide6.QtWidgets import QSizePolicy, QMainWindow, QApplication
from PySide6.QtCore import QPropertyAnimation, QAbstractAnimation, Slot, Signal

class SpoilerWidget(QWidget):
    reveal = Signal(bool)

    def __init__(self, parent=None, title='', animDuration=300):
        super(SpoilerWidget, self).__init__(parent=parent)

        self.MIN_BUTTON_HEIGHT = 20
        self.animDuration = animDuration
        self.toggleAnim = QParallelAnimationGroup()
        self.contentArea = QScrollArea()
        self.hline = QFrame()
        self.btn = QToolButton()
        self.mainLayout = QGridLayout()

        # Setup btn
        btn = self.btn
        btn.setStyleSheet("QToolButton { border: none; }")
        btn.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)

        btn.setArrowType(Qt.RightArrow)
        btn.setText(str(title))
        btn.setCheckable(True)
        btn.setChecked(False)
        btn.setMinimumHeight(self.MIN_BUTTON_HEIGHT)

        # Set styling for hline
        hline = self.hline
        hline.setFrameShape(QFrame.HLine)
        hline.setFrameShadow(QFrame.Sunken)
        hline.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        hline.setMinimumHeight(self.MIN_BUTTON_HEIGHT)

        self.contentArea.setStyleSheet("QScrollArea { background-color: white; border: none; }")
        self.contentArea.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)

        # Start out collapsed
        self.contentArea.setMaximumHeight(0)
        self.contentArea.setMinimumHeight(0)

        # let the entire widget grow and shrink with its content
        toggleAnim = self.toggleAnim

        toggleAnim.addAnimation(QPropertyAnimation(self, b"minimumHeight"))
        toggleAnim.addAnimation(QPropertyAnimation(self, b"maximumHeight"))
        toggleAnim.addAnimation(QPropertyAnimation(self.contentArea, b"maximumHeight"))

        # Removes empty space in layout
        mainLayout = self.mainLayout
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.setSpacing(0)
        mainLayout.setAlignment(Qt.AlignTop)

        row = 0
        mainLayout.addWidget(self.btn, row, 0, 1, 1, Qt.AlignTop)
        mainLayout.addWidget(self.hline, row, 2, 1, 1, Qt.AlignTop)

        # mainLayout.addWidget(self.btn, row, 0, 1, 1)
        # mainLayout.addWidget(self.hline, row, 2, 1, 1)

        # # mainLayout.setAlignment(self.btn, Qt.AlignLeft or Qt.AlignTop)
        # mainLayout.setAlignment(self.btn, Qt.AlignTop)
        # mainLayout.setAlignment(self.hline, Qt.AlignTop)

        row += 1
        mainLayout.addWidget(self.contentArea, row, 0, 1, 3)

        self.setLayout(self.mainLayout)

        # Signal Handlers
        self.btn.clicked.connect(self.toggle_button_anim)

    @Slot(bool)
    def toggle_button_anim(self, checked):
        arrow_type = Qt.DownArrow if checked else Qt.RightArrow
        direction = QAbstractAnimation.Forward if checked else QAbstractAnimation.Backward

        self.btn.setArrowType(arrow_type)
        self.toggleAnim.setDirection(direction)

        self.toggleAnim.start()
        self.reveal.emit(checked)

    def setContentLayout(self, contentLayout, maxWidth, maxHeight):
        self.contentArea.setLayout(contentLayout)
        collapsedHeight = self.sizeHint().height() - self.contentArea.maximumHeight()
        contentHeight = maxHeight

        # Initialize the animations

        # Varies the minimum height of the entire widget
        minHeightAnim = self.toggleAnim.animationAt(0)
        minHeightAnim.setDuration(self.animDuration)
        minHeightAnim.setStartValue(collapsedHeight)
        minHeightAnim.setEndValue(collapsedHeight + contentHeight)

        # Varies the maximum height of the entire widget
        maxHeightAnim = self.toggleAnim.animationAt(1)
        maxHeightAnim.setDuration(self.animDuration)
        maxHeightAnim.setStartValue(collapsedHeight)
        maxHeightAnim.setEndValue(collapsedHeight + contentHeight)

        # Varies the maximum height of the contentArea 
        contentAnimation = self.toggleAnim.animationAt(2)
        contentAnimation.setDuration(self.animDuration)
        contentAnimation.setStartValue(0)
        contentAnimation.setEndValue(contentHeight + collapsedHeight)

# SpoilerWidget Widget Test
class SpoilerWidgetWindowTest(QMainWindow):
    def __init__(self):
        super().__init__()
        self.spoilerwidget = SpoilerWidget()

        # Example Widgets
        self.label = QLabel()
        self.label.setText("Preview Shown")
        self.btn = QPushButton()
        self.btn.setText("Example Button")

        # 1st Method, initialize with a different layout than the default
        layout = QVBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.btn)
        # self.spoilerwidget.setContentLayout(layout)
        self.spoilerwidget.setContentLayout(layout, 1920, 1080)

        # 2nd Method, initialize with the default layout
        # self.spoilerwidget.contentArea.addScrollBarWidget(self.label, Qt.AlignTop)
        # self.spoilerwidget.contentArea.addScrollBarWidget(self.btn, Qt.AlignTop)
        # self.spoilerwidget.setContentLayout(self.spoilerwidget.mainLayout)

        self.setCentralWidget(self.spoilerwidget)

# show_widget(SpoilerWidgetWindowTest)
