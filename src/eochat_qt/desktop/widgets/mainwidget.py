# Imports
from eochat_qt.helpers import Singleton

# Custom Widgets
from eochat_qt.desktop.layouts.borderlayout import BorderLayout, Position
from eochat_qt.desktop.widgets.chatwidget import ChatWidget
from eochat_qt.desktop.widgets.onlinewidget import OnlineWidget
from eochat_qt.desktop.widgets.nearbywidget import NearbyWidget
from eochat_qt.desktop.widgets.previewwidget import PreviewWidget

# PySide6
from PySide6.QtWidgets import (QWidget, QVBoxLayout, QSizePolicy, QTabWidget)
from PySide6.QtGui import Qt

class MainWidget(QWidget):
    __metaclass__ = Singleton

    def __init__(self):
        super().__init__()

        # Initialize widgets
        self.setWindowTitle("Everfree Outpost Chat")
        self.windowlayout = BorderLayout()

        # Players Widget / Left Sidebar Widget
        self.playerswidget = QTabWidget()
        self.onlinewidget = OnlineWidget()
        self.nearbywidget = NearbyWidget()

        # Center Widget
        self.centrewidget = QWidget()
        self.chatwidget = ChatWidget()
        self.previewwidget = PreviewWidget()

        # Players Widget Packing
        self.playerswidget.addTab(self.onlinewidget, "Online")
        self.playerswidget.addTab(self.nearbywidget, "Nearby")

        # Chat Widget Packing
        self.chatwidget.setMinimumSize(860, 600)
        self.chatwidget.setMaximumSize(1440, 900)

        # Center Layout
        mainlayout = QVBoxLayout(self.centrewidget)
        mainlayout.setAlignment(Qt.AlignTop)
        mainlayout.setSpacing(0)

        mainlayout.addWidget(self.previewwidget.btn_bar, alignment=Qt.AlignTop)
        mainlayout.addWidget(self.previewwidget.spoilerwidget, alignment=Qt.AlignTop)
        mainlayout.addWidget(self.chatwidget, alignment=Qt.AlignTop)

        # Center Widget
        self.centrewidget.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        self.centrewidget.setLayout(mainlayout)

        # Add widgets
        self.windowlayout.addWidget(self.centrewidget, Position.Center)
        # self.windowlayout.addWidget(self.onlinewidget, Position.West) # TODO: Align the online players widget to be the same height as the chat widget
        self.windowlayout.addWidget(self.playerswidget, Position.West) # TODO: Align the online players widget to be the same height as the chat widget
        self.setLayout(self.windowlayout)
