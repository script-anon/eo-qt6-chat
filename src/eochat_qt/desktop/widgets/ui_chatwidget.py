# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'chatwidget.ui'
##
## Created by: Qt User Interface Compiler version 6.3.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QGridLayout, QHBoxLayout,
    QLineEdit, QPushButton, QScrollArea, QSizePolicy,
    QSpacerItem, QSplitter, QTextEdit, QWidget)

class Ui_ChatWidget(object):
    def setupUi(self, ChatWidget):
        if not ChatWidget.objectName():
            ChatWidget.setObjectName(u"ChatWidget")
        ChatWidget.resize(860, 640)
        font = QFont()
        font.setPointSize(11)
        ChatWidget.setFont(font)
        self.sa_mainwindow = QScrollArea(ChatWidget)
        self.sa_mainwindow.setObjectName(u"sa_mainwindow")
        self.sa_mainwindow.setGeometry(QRect(30, 40, 800, 400))
        self.sa_mainwindow.setWidgetResizable(True)
        self.sawc_mainwindow = QWidget()
        self.sawc_mainwindow.setObjectName(u"sawc_mainwindow")
        self.sawc_mainwindow.setGeometry(QRect(0, 0, 798, 398))
        self.te_messages = QTextEdit(self.sawc_mainwindow)
        self.te_messages.setObjectName(u"te_messages")
        self.te_messages.setGeometry(QRect(0, 0, 800, 400))
        self.sa_mainwindow.setWidget(self.sawc_mainwindow)
        self.splitter = QSplitter(ChatWidget)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setGeometry(QRect(30, 440, 802, 128))
        self.splitter.setOrientation(Qt.Vertical)
        self.layoutWidget = QWidget(self.splitter)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_2 = QSpacerItem(798, 38, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer_2, 0, 0, 1, 1)

        self.le_input = QLineEdit(self.layoutWidget)
        self.le_input.setObjectName(u"le_input")

        self.gridLayout.addWidget(self.le_input, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(798, 28, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 2, 0, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pb_quit = QPushButton(self.layoutWidget)
        self.pb_quit.setObjectName(u"pb_quit")

        self.horizontalLayout.addWidget(self.pb_quit)

        self.cb_logging = QCheckBox(self.layoutWidget)
        self.cb_logging.setObjectName(u"cb_logging")
        self.cb_logging.setChecked(True)

        self.horizontalLayout.addWidget(self.cb_logging)

        self.cb_enable_sound = QCheckBox(self.layoutWidget)
        self.cb_enable_sound.setObjectName(u"cb_enable_sound")

        self.horizontalLayout.addWidget(self.cb_enable_sound)

        self.horizontalSpacer = QSpacerItem(628, 28, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.gridLayout.addLayout(self.horizontalLayout, 3, 0, 1, 1)

        self.splitter.addWidget(self.layoutWidget)

        self.retranslateUi(ChatWidget)

        QMetaObject.connectSlotsByName(ChatWidget)
    # setupUi

    def retranslateUi(self, ChatWidget):
        ChatWidget.setWindowTitle(QCoreApplication.translate("ChatWidget", u"Form", None))
        self.pb_quit.setText(QCoreApplication.translate("ChatWidget", u"Quit", None))
        self.cb_logging.setText(QCoreApplication.translate("ChatWidget", u"Logging", None))
        self.cb_enable_sound.setText(QCoreApplication.translate("ChatWidget", u"Enable Sound", None))
    # retranslateUi

