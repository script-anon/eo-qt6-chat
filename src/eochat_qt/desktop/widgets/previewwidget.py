# Imports
from eochat_qt.helpers import Singleton

# Custom Widgets
from eochat_qt.desktop.widgets.spoilerwidget import SpoilerWidget

# PySide6
from PySide6.QtCore import QUrl
from PySide6.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout, QPushButton)
from PySide6.QtWebEngineWidgets import QWebEngineView

from PySide6.QtGui import Qt

EVERFREE_OUTPOST_V1_LAUNCHER = 'http://play.everfree-outpost.com/launcher.html#s=http://play.everfree-outpost.com/servers/v1-2021-01-02'

class PreviewWidget(QWidget):
    __metaclass__ = Singleton
    preview_loaded = False
    (MAX_WIDTH, MAX_HEIGHT) = 860, 400
    BTN_BAR_MAX_WIDTH = 200

    def __init__(self):
        super().__init__()
        self.spoilerwidget = SpoilerWidget()
        self.view = QWebEngineView(self);
        self.btn_bar = QWidget()
        self.btn_refresh = QPushButton("Refresh")
        self.btn_close = QPushButton("Close")

        # Layouts
        self.btn_bar_layout = QHBoxLayout()
        self.spoilerlayout = QVBoxLayout(self.spoilerwidget)

        # Preview Widget Buttons
        previewbtnbar_layout = self.btn_bar_layout
        previewbtnbar_layout.addWidget(self.btn_refresh, Qt.AlignTop)
        previewbtnbar_layout.addWidget(self.btn_close, Qt.AlignTop)
        previewbtnbar_layout.setSpacing(6)

        self.btn_bar.setLayout(previewbtnbar_layout)
        self.btn_bar.setMaximumWidth(self.BTN_BAR_MAX_WIDTH)

        # Setup callbacks
        self.btn_close.clicked.connect(self.hide_preview)
        self.btn_refresh.clicked.connect(self.refresh_preview)

        # Preview Widget
        self.spoilerwidget.reveal.connect(self.load_preview)

        self.view.setMaximumWidth(self.MAX_WIDTH)
        self.spoilerwidget.setMaximumWidth(self.MAX_WIDTH)

        # Spoiler Layout
        spoilerlayout = self.spoilerlayout
        spoilerlayout.setAlignment(Qt.AlignTop)
        spoilerlayout.addWidget(self.view, Qt.AlignTop)
        spoilerlayout.setSpacing(0)

        self.spoilerwidget.setContentLayout(spoilerlayout, self.MAX_WIDTH, self.MAX_HEIGHT)
        self.view.setLayout(spoilerlayout)

    def load_preview(self):
        if (not self.preview_loaded):
            self.refresh_preview()
            self.preview_loaded = True

    def hide_preview(self):
        if (self.preview_loaded):
            self.view.close();
            self.preview_loaded = False

    def refresh_preview(self):
        self.view.load(QUrl(EVERFREE_OUTPOST_V1_LAUNCHER));
