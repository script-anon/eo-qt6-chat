# Imports
from eochat_qt.helpers import Singleton
from eochat_qt.desktop.widgets.ui_nearbywidget import Ui_NearbyWidget

# PySide6
from PySide6.QtCore import Signal, Slot
from PySide6.QtWidgets import QWidget

PLAYER_NEARBY   = 0x800c
PLAYER_FARAWAY  = 0x800d

class Players:
    ''' Players data structure for dealing with players
    Allows constant lookup with get_players_id(), and
    push/pop functionality
    '''
    def __init__(self):
        self.index = 0
        self.players_dict: dict[str,int] = {}
        self.players_list: list[str] = []

    def get_player_id(self, name: str):
        found = self.players_dict.__contains__(name)
        if (found):
            return self.players_dict[name]
        else:
            return -1

    def get_players(self):
        return self.players_list

    def push_player(self, name: str):
        self.index += 1
        self.players_dict[name] = self.index
        self.players_list.append(name)

    def pop_player(self, name: str):
        self.players_list.remove(name)

class NearbyWidget(QWidget):
    __metaclass__ = Singleton
    sent_ping = Signal(bool)

    def __init__(self):
        super().__init__()
        self.ui = Ui_NearbyWidget()
        self.ui.setupUi(self)
        # self.nearby = list(str())
        self.nearby = Players()

        self.sent_ping.connect(self.show_online_players)

    @Slot(bool)
    def show_online_players(self):
        ''' Show all online player's names
        This slot listens for ow_sent_ping signals, and redraws/updates the online players list '''
        names = '\n'.join(self.nearby.get_players())
        self.ui.te_nearby.setText(names)

    def update_nearby_players_list(self):
        ''' Force the widget to update the nearby players list '''
        self.sent_ping.emit(True)
