# Imports
from eochat_qt.helpers import Singleton
from eochat_qt.desktop.widgets.ui_onlinewidget import Ui_OnlineWidget

# PySide6
from PySide6.QtWidgets import QWidget
from PySide6.QtCore import QObject, Slot, Signal

class OnlineMBus(QObject):
    ow_sent_ping = Signal(bool)

class OnlineWidget(QWidget):
    __metaclass__ = Singleton

    def __init__(self):
        super().__init__()
        self.ui = Ui_OnlineWidget()
        self.ui.setupUi(self)

        self.online = list(str())
        self.ombus = OnlineMBus()

        # When we ping for the players, show the online player names in the widget
        self.ombus.ow_sent_ping.connect(self.show_online_players)

    @Slot(bool)
    def show_online_players(self):
        ''' Show all online player's names
        This slot listens for ow_sent_ping signals, and redraws/updates the online players list '''
        names = '\n'.join(self.online)
        self.ui.te_online.setText(names)

    def update_online_players_list(self):
        ''' Force the widget to update the online players list '''
        self.ombus.ow_sent_ping.emit(True)
