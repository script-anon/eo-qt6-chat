# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'nearbywidget.ui'
##
## Created by: Qt User Interface Compiler version 6.3.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QLabel, QLayout,
    QSizePolicy, QTextEdit, QVBoxLayout, QWidget)

class Ui_NearbyWidget(object):
    def setupUi(self, NearbyWidget):
        if not NearbyWidget.objectName():
            NearbyWidget.setObjectName(u"NearbyWidget")
        NearbyWidget.resize(300, 400)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(NearbyWidget.sizePolicy().hasHeightForWidth())
        NearbyWidget.setSizePolicy(sizePolicy)
        NearbyWidget.setMaximumSize(QSize(16777215, 16777215))
        font = QFont()
        font.setPointSize(11)
        NearbyWidget.setFont(font)
        self.gridLayout = QGridLayout(NearbyWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.vl_nearby = QVBoxLayout()
        self.vl_nearby.setObjectName(u"vl_nearby")
        self.lbl_nearby = QLabel(NearbyWidget)
        self.lbl_nearby.setObjectName(u"lbl_nearby")
        self.lbl_nearby.setAlignment(Qt.AlignCenter)

        self.vl_nearby.addWidget(self.lbl_nearby, 0, Qt.AlignTop)

        self.te_nearby = QTextEdit(NearbyWidget)
        self.te_nearby.setObjectName(u"te_nearby")
        self.te_nearby.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.vl_nearby.addWidget(self.te_nearby)


        self.gridLayout.addLayout(self.vl_nearby, 0, 0, 1, 1)


        self.retranslateUi(NearbyWidget)

        QMetaObject.connectSlotsByName(NearbyWidget)
    # setupUi

    def retranslateUi(self, NearbyWidget):
        NearbyWidget.setWindowTitle(QCoreApplication.translate("NearbyWidget", u"Form", None))
        self.lbl_nearby.setText(QCoreApplication.translate("NearbyWidget", u"Nearby Players", None))
    # retranslateUi

