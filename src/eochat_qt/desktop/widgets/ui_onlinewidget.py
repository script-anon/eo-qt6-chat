# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'onlinewidget.ui'
##
## Created by: Qt User Interface Compiler version 6.3.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QLabel, QLayout,
    QSizePolicy, QTextEdit, QVBoxLayout, QWidget)

class Ui_OnlineWidget(object):
    def setupUi(self, OnlineWidget):
        if not OnlineWidget.objectName():
            OnlineWidget.setObjectName(u"OnlineWidget")
        OnlineWidget.resize(300, 400)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(OnlineWidget.sizePolicy().hasHeightForWidth())
        OnlineWidget.setSizePolicy(sizePolicy)
        OnlineWidget.setMaximumSize(QSize(16777215, 16777215))
        font = QFont()
        font.setPointSize(11)
        OnlineWidget.setFont(font)
        self.gridLayout = QGridLayout(OnlineWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.vl_online = QVBoxLayout()
        self.vl_online.setObjectName(u"vl_online")
        self.lbl_online = QLabel(OnlineWidget)
        self.lbl_online.setObjectName(u"lbl_online")
        self.lbl_online.setAlignment(Qt.AlignCenter)

        self.vl_online.addWidget(self.lbl_online, 0, Qt.AlignTop)

        self.te_online = QTextEdit(OnlineWidget)
        self.te_online.setObjectName(u"te_online")
        self.te_online.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)

        self.vl_online.addWidget(self.te_online)


        self.gridLayout.addLayout(self.vl_online, 0, 0, 1, 1)


        self.retranslateUi(OnlineWidget)

        QMetaObject.connectSlotsByName(OnlineWidget)
    # setupUi

    def retranslateUi(self, OnlineWidget):
        OnlineWidget.setWindowTitle(QCoreApplication.translate("OnlineWidget", u"Form", None))
        self.lbl_online.setText(QCoreApplication.translate("OnlineWidget", u"Online Players", None))
    # retranslateUi

