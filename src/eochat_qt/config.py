from eochat_qt.helpers import *

import os
import shutil

# Definitions

# Platforms
is_win = is_windows()

# Logging
LOG_CFG_PATH_WINDOWS    = r'%APPDATA%\eochat\eo_chat-qt\logs'
LOG_CFG_PATH_LINUX      = '~/.config/eochat/eo_chat-qt/logs'

# Sound
NOTIFICATION_SOUND      = "resources/file-sounds-1148-juntos.mp3"
NOTIFICATION_SOUND_WAV  = "resources/file-sounds-1148-juntos.wav"

def override_value(default, cmp, override):
    res = default
    if cmp:
        res = override
    return res

def default_to(default, override):
    return override_value(default, override is not None, override)

def cfg_default_to(cfg, default, val):
    return cfg.get(default) if cfg.__contains__(default) else val

def toggle_chat_logging(cfg):
    logging = cfg_default_to(cfg, 'logging', True) # Toggle logging by default
    return logging

def get_log_name(log_name):
    global LOG_CFG_PATH_LINUX, LOG_CFG_PATH_WINDOWS
    logdir = override_value(expand(LOG_CFG_PATH_LINUX), is_win, expand(LOG_CFG_PATH_WINDOWS))
    if (not logdir.exists()):
        os.makedirs(logdir, exist_ok = True)
    log_name = f'{logdir}\\{log_name}.txt' if is_win else f'{logdir}/{log_name}.txt'
    return log_name

def parse_user_cfg():
    ''' Parse the user's config file '''
    CFG_PATH_WINDOWS    = r'%APPDATA%\eochat\config.toml'
    CFG_PATH_LINUX      = '~/.config/eochat/config.toml'

    cfgfp = override_value(expand(CFG_PATH_LINUX), is_win, expand(CFG_PATH_WINDOWS))

    # If the user config file does not exist
    if (not cfgfp.exists()):
        # Copy our example config over to where it should exist
        os.makedirs(os.path.dirname(cfgfp), exist_ok = True)
        shutil.copyfile(f'{Path(__file__).resolve().parents[2]}/config.toml', cfgfp)

    return read_cfg(cfgfp)

def parse_user_eochat_cfg():
    cfg = parse_user_cfg()
    cfg_eochat = cfg_default_to(cfg, 'eochat', cfg)
    return cfg_eochat

def parse_user_eochat_qt_cfg():
    cfg = parse_user_cfg()
    cfg_eochat_qt = cfg_default_to(cfg, 'eochat-qt', cfg)
    return cfg_eochat_qt

def parse_user_appearance(cfg):
    DEFAULT_APPEARANCE = [0,1, 2,2,0, 6,6,6, 0,0,0, 0,0,0]
    appearance = cfg_default_to(cfg, 'appearance', DEFAULT_APPEARANCE)
    return appearance

def parse_user_tsformat(cfg):
    DEFAULT_TIMESTAMP_FMT = '"%I:%M %p"' # 10:30 AM
    fmt = cfg_default_to(cfg, 'tsformat', DEFAULT_TIMESTAMP_FMT)
    return fmt

def parse_user_logformat(cfg):
    DEFAULT_TIMESTAMP_FMT   = '%Y-%m-%d %H:%M:%S' # '2022-08-25 22:27:08'
    DEFAULT_LOG_FILE_FMT    = '%Y-%m-%d'

    TIMESTAMP_FMT   = DEFAULT_TIMESTAMP_FMT
    LOG_FILE_FMT    = DEFAULT_LOG_FILE_FMT

    if ((cfg.__contains__('log')) and ((cfg_log := cfg.get('log')) is not None)):
        TIMESTAMP_FMT = cfg_default_to(cfg_log, 'tsformat', '')
        LOG_FILE_FMT = cfg_default_to(cfg_log, 'fpformat', '')

    log_name = f'chat-{timestamp(LOG_FILE_FMT)}'
    logfp = get_log_name(log_name)
    return log_name, logfp, LOG_FILE_FMT, TIMESTAMP_FMT

def parse_user_notify_sound(cfg):
    global NOTIFICATION_SOUND

    enable_sound = cfg_default_to(cfg, 'enable-sound', False)
    notify_sound = cfg_default_to(cfg, 'notify-sound', NOTIFICATION_SOUND)
    return enable_sound, notify_sound

def parse_user_creds(cfg):
    ''' Parse the user's login credentials '''
    # Defaults
    user = cfg_default_to(cfg, 'user', '')
    password = cfg_default_to(cfg, 'password', '')

    # Read environment variables
    user = default_to(user, os.environ.get('OUTPOST_USER'))
    password = default_to(user, os.environ.get('OUTPOST_PASSWORD'))
    return user, password

def parse_user_host(cfg):
    host        = default_to('main', cfg.get('host'))
    which       = override_value(True, host == 'main', False)

    AUTH_HOST   = override_value('auth.everfree-outpost.com', which, 'localhost:5000')
    AUTH_BASE   = f'https://{AUTH_HOST}'
    AUTH_ORIGIN = override_value('http://play.everfree-outpost.com', which, 'http://localhost:8889')
    GAME_URL    = override_value('ws://game2.everfree-outpost.com:8888/ws', which, 'ws://localhost:8888/ws')

    return AUTH_HOST, AUTH_BASE, AUTH_ORIGIN, GAME_URL

class LogConfig(metaclass=Singleton):
    def __init__(self, cfg):
        self.tsformat        = parse_user_tsformat(cfg)
        self.log_name, self.logfp, self.LOG_FILE_FMT, self.LOG_TIMESTAMP_FMT = parse_user_logformat(cfg)
        self.logging         = toggle_chat_logging(cfg)

class EochatConfig(metaclass=Singleton):
    def __init__(self):
        self.is_win                 = is_win
        self.cfg                    = parse_user_cfg()
        self.cfg_eochat             = parse_user_eochat_cfg()
        self.cfg_eochat_qt          = parse_user_eochat_qt_cfg()
        self.user, self.password    = parse_user_creds(self.cfg_eochat)
        self.appearance             = parse_user_appearance(self.cfg_eochat)
        self.enable_sound, self.notify_sound = parse_user_notify_sound(self.cfg_eochat)
        self.AUTH_HOST, self.AUTH_BASE, self.AUTH_ORIGIN, self.GAME_URL = parse_user_host(self.cfg_eochat)

# Singletons

# Declare global configuration variables
CONFIG      = EochatConfig()
LOG_CONFIG  = LogConfig(CONFIG.cfg_eochat_qt)
