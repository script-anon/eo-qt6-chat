PATHR=resources
PATHW=src/eochat_qt/desktop/widgets

P6UIC:=pyside6-uic

# If building with pipenv, run the command through pipenv instead
# Usage: make make pv
ifeq ($(filter pv,$(MAKECMDGOALS)),pv)
P6UIC:=pipenv run pyside6-uic
endif

# Fake targets
.PHONY: pv

make:
	$(P6UIC) $(PATHR)/chatwidget.ui > $(PATHW)/ui_chatwidget.py 
	$(P6UIC) $(PATHR)/onlinewidget.ui > $(PATHW)/ui_onlinewidget.py 
	$(P6UIC) $(PATHR)/nearbywidget.ui > $(PATHW)/ui_nearbywidget.py 

clean:
	rm -f $(PATHW)/ui_*.py
